# Get the SNS Topic ARN 
$TopicArn = `
aws sns list-topics `
--output json `
--query 'Topics[0].TopicArn'

# Get the SNS Subs ARN 
$EMAIL = `
aws sns list-subscriptions-by-topic `
--topic-arn $TopicArn `
--output json `
--query 'Subscriptions[0].Endpoint'

# Save it in a .env file as documented in: https://docs.gitlab.com/ee/ci/variables/#pass-an-environment-variable-to-another-job
$KEY='EMAIL='
$ENV = $KEY + $EMAIL 
Add-Content -Path build.env -Value $ENV
