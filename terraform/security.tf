# Lambda Role
resource "aws_iam_role" "lambdas_role" {
  name = "${var.resource_name[0]}_lambda_role"

  assume_role_policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Principal" : {
          "Service" : "lambda.amazonaws.com"
        },
        "Action" : "sts:AssumeRole"
      }
    ]
  })
}

# Policies attached to the lambda role
resource "aws_iam_policy_attachment" "attach_lambda_policy" {
  name       = "${var.resource_name[0]}_lambda_execution_policy"
  roles      = [aws_iam_role.lambdas_role.name]
  policy_arn = data.aws_iam_policy.AWSLambdaBasicExecutionRole.arn
}

resource "aws_iam_policy_attachment" "attach_stepf_policy" {
  name       = "${var.resource_name[0]}_lambda_stepf_policy"
  roles      = [aws_iam_role.lambdas_role.name]
  policy_arn = data.aws_iam_policy.AWSStepFunctionsFullAccess.arn
}

# Step Function Role
resource "aws_iam_role" "stepf_role" {
  name = var.resource_name[3]

  assume_role_policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Principal" : {
          "Service" : "states.amazonaws.com"
        },
        "Action" : "sts:AssumeRole"
      }
    ]
  })
}

# Step Functions Policies
resource "aws_iam_policy" "stepf_sns_access_policy" {
  name        = "${var.resource_name[0]}_stepf_sns_access"
  description = "Allows AWS Step Functions to publish to SNS"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = [
          "sns:Publish"
        ],
        Resource = [
          "arn:aws:sns:${data.aws_region.aws_user_region.name}:${data.aws_caller_identity.aws_user_info.account_id}:${var.resource_name[1]}"
        ]
      }
    ]
  })
}

resource "aws_iam_policy" "stepf_xray_access_policy" {
  name        = "${var.resource_name[0]}_stepf_xray_access"
  description = "Allows AWS Step Functions to call to XRay"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = [
          "xray:PutTraceSegments",
          "xray:PutTelemetryRecords",
          "xray:GetSamplingRules",
          "xray:GetSamplingTargets"
        ],
        Resource = [
          "*"
        ]
      }
    ]
  })
}

# Policies attached to the step function role
resource "aws_iam_policy_attachment" "attach_sns_policy" {
  name       = "${var.resource_name[0]}_stepf_sns_policy"
  roles      = [aws_iam_role.stepf_role.name]
  policy_arn = aws_iam_policy.stepf_sns_access_policy.arn
}

resource "aws_iam_policy_attachment" "attach_xray_policy" {
  name       = "${var.resource_name[0]}_stepf_xray_policy"
  roles      = [aws_iam_role.stepf_role.name]
  policy_arn = aws_iam_policy.stepf_xray_access_policy.arn
}