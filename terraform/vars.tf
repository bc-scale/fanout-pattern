variable "resource_name" {
  description = "Name of the resources created to finding them more easily in the AWS Console"
  type        = list(string)
  default = [
    "fanout_pattern",
    "DeliveryRequest",
    "ProcessNewDelivery",
    "delivery-state-machine-role"
  ]
}

variable "sub_email_endpoint" {
  description = "email which will receive the SNS notifications"
  default     = "email@xxxxemail.com"
}

variable "lambdas_name" {
  description = "Lambdas name dictionary to iterate in with teh terraform's lambda function"
  type        = map(string)
  default = {
    "driverSally" = "01_driverSally.zip",
    "driverAlex"  = "02_driverAlex.zip"
  }
}
