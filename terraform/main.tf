# Serveless sub/pub service SNS
resource "aws_sns_topic" "sns_topic" {
  name = var.resource_name[1]
}

resource "aws_sns_topic_subscription" "sns_topic_sub" {
  topic_arn = aws_sns_topic.sns_topic.arn
  protocol  = "email"
  endpoint  = var.sub_email_endpoint
}

# Lambda functions
resource "aws_lambda_function" "lambda_function" {
  for_each      = var.lambdas_name
  filename      = "../functions/${each.value}"
  function_name = each.key
  role          = aws_iam_role.lambdas_role.arn
  handler       = "index.handler"
  runtime       = "nodejs16.x"

  tags = {
    Name = "${each.key}"
  }
}

# Step function 
resource "aws_sfn_state_machine" "sfn_state_machine" {
  name     = var.resource_name[2]
  role_arn = aws_iam_role.stepf_role.arn
  type     = "STANDARD"

  definition = <<EOF
{
    "StartAt": "Check Inventory",
    "States": {
        "Check Inventory": {
            "Type": "Pass",
            "OutputPath": "$",
            "Next": "Place CC Hold"
        },
        "Place CC Hold": {            
            "Type": "Pass",
            "OutputPath": "$",
            "Next": "Request Driver"
        },
        "Request Driver": {
            "Type": "Task",
            "Resource": "arn:aws:states:::sns:publish.waitForTaskToken",
            "TimeoutSeconds": 15,
            "Parameters": {
                "Message": {
                    "TaskToken.$": "$$.Task.Token",
                    "Input.$": "$.OrderNumber"
                },
                "TopicArn": "${aws_sns_topic.sns_topic.arn}"
            },
             "Catch": [ {
                "ErrorEquals": [ "States.Timeout" ],
                "Next": "Notify Customer of Delay"
             } ],
            "Next": "Delivery Assigned to Driver"
        },
        "Delivery Assigned to Driver": {
            "Type": "Pass",
            "OutputPath": "$",
            "End": true
        },
        "Notify Customer of Delay": {
            "Type": "Pass",
            "OutputPath": "$",
            "End": true
        }
    }
}
EOF
}